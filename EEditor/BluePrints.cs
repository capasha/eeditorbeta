﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EEditor
{
    public partial class BluePrints : Form
    {
        private Bitmap img4 = new Bitmap(25 * 16, 25 * 16);
        private Bitmap img1 = new Bitmap(25 * 16, 25 * 16);
        private List<BlockData> blocks = new List<BlockData>();

        public BluePrints()
        {
            InitializeComponent();
            pictureBox1.Width = 25 * 16;
            pictureBox1.Height = 25 * 16;
        }

        private void BluePrints_Load(object sender, EventArgs e)
        {
            if (Clipboard.ContainsData("EEBlueprints"))
            {

                string[][,] data = (string[][,])Clipboard.GetData("EEBlueprints");
                if (data?.Length == 9)
                {
                    using (Graphics gr = Graphics.FromImage(img4))
                    {
                        gr.Clear(Color.Gray);
                    }
                    for (int y = 0; y < data[1].GetLength(0); y++)
                    {
                        for (int x = 0; x < data[1].GetLength(1); x++)
                        {
                            int bid1 = Convert.ToInt32(data[1][y, x]);

                            
                            if (bid1 >= 500 && bid1 <= 999)
                            {
                                if (MainForm.backgroundBMI[bid1] != 0 || bid1 == 500)
                                {
                                    blocks.Add(new BlockData() { BlockID = bid1, Layer = 1, X = x, Y = y });
                                    using (Graphics gr1 = Graphics.FromImage(img4))
                                    {
                                        gr1.DrawImage(MainForm.backgroundBMD.Clone(new Rectangle(MainForm.backgroundBMI[bid1] * 16, 0, 16, 16), MainForm.backgroundBMD.PixelFormat), x * 16, y * 16);
                                    }
                                }
                            }
                        }

                    }
                    for (int y = 0; y < data[0].GetLength(0); y++)
                    {
                        for (int x = 0; x < data[0].GetLength(1); x++)
                        {
                            int bid = Convert.ToInt32(data[0][y, x]);

                            using (Graphics gr = Graphics.FromImage(img4))
                            {
                                if (bid < 500 || bid >= 1001)
                                {
                                    if (MainForm.decosBMI[bid] != 0)
                                    {
                                        img1 = bdata.getRotation(bid, Convert.ToInt32(data[2][y, x]));
                                        if (img1 != null)
                                        {
                                            gr.DrawImage(img1, new Rectangle(x * 16, y * 16, 16, 16));
                                            blocks.Add(new BlockData() { BlockID = bid, Layer = 0, X = x, Y = y, Param = new object[] { Convert.ToInt32(data[2][y, x]) } });
                                        }
                                        else 
                                        {
                                            blocks.Add(new BlockData() { BlockID = bid, Layer = 0, X = x, Y = y });
                                            gr.DrawImage(MainForm.decosBMD.Clone(new Rectangle(MainForm.decosBMI[Convert.ToInt32(data[0][y, x])] * 16, 0, 16, 16), MainForm.decosBMD.PixelFormat), x * 16, y * 16);
                                        }
                                    }
                                    else if (MainForm.miscBMI[bid] != 0 || bid == 119)
                                    {
                                        img1 = bdata.getRotation(bid, Convert.ToInt32(data[2][y, x]));
                                        if (img1 != null)
                                        {
                                            gr.DrawImage(img1, new Rectangle(x * 16, y * 16, 16, 16));
                                            blocks.Add(new BlockData() { BlockID = bid, Layer = 0, X = x, Y = y, Param = new object[] { Convert.ToInt32(data[2][y, x]) } });
                                        }
                                        else
                                        {
                                            blocks.Add(new BlockData() { BlockID = bid, Layer = 0, X = x, Y = y });
                                            gr.DrawImage(MainForm.miscBMD.Clone(new Rectangle(MainForm.miscBMI[Convert.ToInt32(data[0][y, x])] * 16, 0, 16, 16), MainForm.miscBMD.PixelFormat), x * 16, y * 16);
                                        }
                                    }
                                    else if (bid != 0)
                                    {
                                        blocks.Add(new BlockData() { BlockID = bid, Layer = 0, X = x, Y = y });
                                        gr.DrawImage(MainForm.foregroundBMD.Clone(new Rectangle(MainForm.foregroundBMI[Convert.ToInt32(data[0][y, x])] * 16, 0, 16, 16), MainForm.foregroundBMD.PixelFormat), x * 16, y * 16);
                                    }
                                }
                            }
                        }
                    }
                    using (Graphics gr = Graphics.FromImage(img4))
                    {
                        gr.DrawRectangle(new Pen(Color.Black), new Rectangle(0, 0, data[0].GetLength(1) * 16 - 1, data[0].GetLength(0) * 16 - 1));
                    }
                    //Clipboard.Clear();
                    //Console.WriteLine(data[0]);
                    pictureBox1.Image = img4;

                }
                //;
                //Console.WriteLine(data[0]);
                Clipboard.Clear();

            }
            // Clipboard.Clear();
        }

        private void BluePrints_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void SaveBPButton_Click(object sender, EventArgs e)
        {
            foreach (var val in blocks)
            {

            }
            File.WriteAllText($"{Directory.GetCurrentDirectory()}\\test.json", JsonConvert.SerializeObject(blocks, Newtonsoft.Json.Formatting.Indented));
        }

        private void LoadButton_Click(object sender, EventArgs e)
        {
            List<object> obj = new List<object>();
            if (File.Exists($"{Directory.GetCurrentDirectory()}\\test.json"))
            {
                var serializer = new JsonSerializer();

                using (var sw = new StreamReader($"{Directory.GetCurrentDirectory()}\\test.json"))
                using (var reader = new JsonTextReader(sw))
                {
                    obj.Add(serializer.Deserialize(reader));
                }
            }
        }
        public static object DeserializeFromStream(Stream stream)
        {
            var serializer = new JsonSerializer();

            using (var sr = new StreamReader(stream))
            using (var jsonTextReader = new JsonTextReader(sr))
            {
                return serializer.Deserialize(jsonTextReader);
            }
        }

    }
    public class BlockData
    {
        public int BlockID { get; set; }
        public int Layer { get; set; }

        public int X { get; set; }

        public int Y { get; set; }

        public object[] Param { get; set; }
    }
}
